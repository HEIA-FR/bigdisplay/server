// Copyright 2018 Jacques Supcik
// Haute école d'ingénierie et d'architecture de Fribourg
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"github.com/pkg/errors"
)

var segment map[rune]int

func init() {
	const (
		a  = 1 << (7 - 0)
		b  = 1 << (7 - 6)
		c  = 1 << (7 - 5)
		d  = 1 << (7 - 4)
		e  = 1 << (7 - 3)
		f  = 1 << (7 - 1)
		g  = 1 << (7 - 2)
		dp = 1 << (7 - 7) // nolint
	)

	segment = map[rune]int{
		'1': b | c,
		'2': a | b | d | e | g,
		'3': a | b | c | d | g,
		'4': b | c | f | g,
		'5': a | c | d | f | g,
		'6': a | c | d | e | f | g,
		'7': a | b | c,
		'8': a | b | c | d | e | f | g,
		'9': a | b | c | d | f | g,
		'0': a | b | c | d | e | f,
		' ': 0,
		'-': g,
		'A': a | b | c | e | f | g,
		'B': a | b | c | d | e | f | g,
		'b': c | d | e | f | g,
		'C': a | d | e | f,
		'c': d | e | g,
		'D': a | b | c | d | e | f,
		'd': b | c | d | e | g,
		'E': a | d | e | f | g,
		'F': a | e | f | g,
		'f': a | e | f | g,
		'G': a | c | d | e | f | g,
		'H': b | c | e | f | g,
		'h': c | e | f | g,
		'I': e | f,
		'i': e,
		'L': d | e | f,
		'l': e | f,
		'O': a | b | c | d | e | f,
		'o': c | d | e | g,
		'S': a | c | d | f | g,
		'U': b | c | d | e | f,
		'u': c | d | e,
		'V': b | c | d | e | f,
		'v': c | d | e,
		'_': d,
		'.': dp,
	}
}

func textToSegments(text string, dispLen int) ([]int, error) {
	t := []rune(text)
	result := make([]int, dispLen)

	j := len(result) - 1
	for i := len(t) - 1; i >= 0; i-- {
		if t[i] == '.' {
			result[j] |= segment['.']
		} else if val, ok := segment[t[i]]; ok {
			if j < 0 {
				return nil, errors.New("String too long")
			}
			result[j] |= val
			j--
		} else {
			return nil, errors.New("Invalid character")
		}
	}
	return result, nil
}
