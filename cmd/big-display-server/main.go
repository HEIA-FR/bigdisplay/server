// Copyright 2018 Jacques Supcik
// Haute école d'ingénierie et d'architecture de Fribourg
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"flag"
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/heia-fr/bigdisplay/server"

	"github.com/pkg/errors"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	log "github.com/sirupsen/logrus"
)

func bootSequence(disp *server.BigDisplay) error {
	for i := 0; i < disp.DisplayLen; i++ {
		msg := "8." + strings.Repeat(" ", disp.DisplayLen-1-i)
		if err := disp.WriteText(msg, false); err != nil {
			return err
		}
		time.Sleep(300 * time.Millisecond)
	}
	if err := disp.WriteText("", false); err != nil {
		return err
	}
	time.Sleep(500 * time.Millisecond)

	msg := strings.Repeat("8.", disp.DisplayLen)
	if err := disp.WriteText(msg, false); err != nil {
		return err
	}
	time.Sleep(1000 * time.Millisecond)
	return disp.WriteText("", false)
}

func main() { // nolint: gocyclo
	debug := flag.Bool("debug", false, "Run in debug mode")
	broker := flag.String("broker", "tcp://127.0.0.1:1883", "MQTT Broker URI")
	user := flag.String("username", "", "MQTT Username")
	password := flag.String("password", "", "MQTT Password")
	topic := flag.String("topic", "message", "Topic name from which to subscribe")
	qos := flag.Int("qos", 0, "Quality of Service 0,1,2 (default 0)")
	skipTest := flag.Bool("skip-test", false, "Skip Test sequence on boot")
	stallNotification := flag.Int("stall-notification", 5, "Stall notification (in seconds)")

	dataPin := flag.String("data-pin", "GPIO2", "Data pin")
	clockPin := flag.String("clock-pin", "GPIO3", "Clock pin")
	latchPin := flag.String("latch-pin", "GPIO17", "Latch pin")
	displayLen := flag.Int("display-len", 6, "Number of digits")

	flag.Parse()

	if *debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}

	log.Debug("Creating Display")
	disp, err := server.NewBigDisplay(*dataPin, *clockPin, *latchPin, *displayLen)
	if err != nil {
		panic(errors.WithMessage(err, "Unable to create display"))
	}

	if !*skipTest {
		err = bootSequence(disp)
		if err != nil {
			panic(errors.WithMessage(err, "Unable to start Display"))
		}
	}

	opts := MQTT.NewClientOptions()
	opts.AddBroker(*broker)
	opts.SetUsername(*user)
	opts.SetPassword(*password)

	choke := make(chan [2]string)

	opts.SetDefaultPublishHandler(func(client MQTT.Client, msg MQTT.Message) {
		choke <- [2]string{msg.Topic(), string(msg.Payload())}
	})

	client := MQTT.NewClient(opts)

	log.Debug("Connecting to MQTT Broker")
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	defer client.Disconnect(250)

	if token := client.Subscribe(*topic, byte(*qos), nil); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		os.Exit(1)
	}

	var tickerChannel <-chan time.Time
	var ticker *time.Ticker

	if *stallNotification > 0 {
		ticker = time.NewTicker(time.Duration(*stallNotification) * time.Second)
		tickerChannel = ticker.C
	} else {
		ticker = nil
		tickerChannel = nil
	}

	currentText := ""
	for {
		select {
		case <-tickerChannel:
			err := disp.WriteText(currentText, true)
			if err != nil {
				log.Error(err)
			}
		case incoming := <-choke:
			if ticker != nil {
				ticker.Stop()
				ticker = time.NewTicker(time.Duration(*stallNotification) * time.Second)
				tickerChannel = ticker.C
			}
			log.Debugf("Received '%v' from topic '%v'", incoming[1], incoming[0])
			currentText = incoming[1]
			err := disp.WriteText(currentText, false)
			if err != nil {
				log.Error(err)
			}
		}
	}
}
