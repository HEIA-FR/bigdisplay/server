#!/bin/bash

mkdir dist
for armv in 5 6 7; do
  env GOOS=linux GOARCH=arm GOARM=${armv} go build -o dist/big-display-server-arm${armv}
  env GOOS=linux GOARCH=arm GOARM=${armv} go build -o dist/big-display-server-s-arm${armv} -ldflags="-s -w"
done
