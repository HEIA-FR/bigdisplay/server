[![GoDoc](https://godoc.org/gitlab.com/HEIA-FR/bigdisplay/server?status.svg)](https://godoc.org/gitlab.com/HEIA-FR/bigdisplay/server)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/HEIA-FR/bigdisplay/server)](https://goreportcard.com/report/gitlab.com/HEIA-FR/bigdisplay/server)
[![license](https://img.shields.io/badge/license-Apache--2.0-green.svg)](https://gitlab.com/HEIA-FR/bigdisplay/server)

# BigDisplay

BigDisplay is a demo project for the [Haute école d'ingénierie et d'architecture de Fribourg](https://www.heia-fr.ch/). It manages a large, 6 digits 7-segments display.

## References:

* [Raspberry Pi Zero](https://www.raspberrypi.org/products/raspberry-pi-zero/)
* [Large Digit Driver](https://www.sparkfun.com/products/13279)
* [7-Segment Display - 6.5 (Red)](https://www.sparkfun.com/products/8530)
* [Logic Level Converter Bi-Directional](https://www.play-zone.ch/de/play-zone-logic-level-converter-bi-directional.html)
