// Copyright 2018 Jacques Supcik
// Haute école d'ingénierie et d'architecture de Fribourg
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"github.com/pkg/errors"
	"periph.io/x/periph/conn/gpio"
	"periph.io/x/periph/conn/gpio/gpioreg"
	"periph.io/x/periph/host"
)

// BigDisplay is the base structure for the display
type BigDisplay struct {
	dataPin    gpio.PinOut
	clockPin   gpio.PinOut
	latchPin   gpio.PinOut
	DisplayLen int // Length (# of digits) of the display
}

// NewBigDisplay returns a new instance of BigDisplay
func NewBigDisplay(dataPin string, clockPin string, latchPin string, displayLen int) (*BigDisplay, error) {
	_, err := host.Init()
	if err != nil {
		return nil, errors.WithMessage(err, "Unable to initialize GPIO")
	}

	bd := BigDisplay{}

	p := gpioreg.ByName(dataPin)
	if p == nil {
		return nil, errors.Errorf("Failed to find %v", dataPin)
	}
	bd.dataPin = p

	p = gpioreg.ByName(clockPin)
	if p == nil {
		return nil, errors.Errorf("Failed to find %v", clockPin)
	}
	bd.clockPin = p

	p = gpioreg.ByName(latchPin)
	if p == nil {
		return nil, errors.Errorf("Failed to find %v", latchPin)
	}
	bd.latchPin = p

	if err := bd.dataPin.Out(gpio.High); err != nil {
		return nil, errors.WithMessage(err, "Can't init data pin to high")
	}

	if err := bd.clockPin.Out(gpio.High); err != nil {
		return nil, errors.WithMessage(err, "Can't init clock pin to high")
	}

	if err := bd.latchPin.Out(gpio.High); err != nil {
		return nil, errors.WithMessage(err, "Can't init latch pin to high")
	}

	bd.DisplayLen = displayLen

	return &bd, nil
}

// pushDigit inserts a digit on the display
func (bd *BigDisplay) pushDigit(digit int) error {
	for i := 0; i < 8; i++ {
		var data gpio.Level
		if digit&(1<<uint(i)) != 0 {
			data = gpio.High
		} else {
			data = gpio.Low
		}

		if err := bd.clockPin.Out(gpio.Low); err != nil {
			return errors.WithMessage(err, "Can't set clock pin to low")
		}
		if err := bd.dataPin.Out(data); err != nil {
			return errors.WithMessage(err, "Can't drive data pin")
		}
		if err := bd.clockPin.Out(gpio.High); err != nil {
			return errors.WithMessage(err, "Can't set clock pin back to high")
		}
	}
	return nil
}

// refresh sends the pushed data to the display
func (bd *BigDisplay) refresh() error {
	if err := bd.latchPin.Out(gpio.Low); err != nil {
		return errors.WithMessage(err, "Can't set latch pin to low")
	}
	if err := bd.latchPin.Out(gpio.High); err != nil {
		return errors.WithMessage(err, "Can't set latch pin back to high")
	}
	return nil
}

// WriteText writes the text to the display
func (bd *BigDisplay) WriteText(text string, dots bool) error {
	data, err := textToSegments(text, bd.DisplayLen)
	if err != nil {
		return errors.WithMessage(err, "Unable to write text")
	}
	for i := len(data) - 1; i >= 0; i-- {
		seg := data[i]
		if dots {
			seg |= segment['.']
		}
		if err = bd.pushDigit(seg); err != nil {
			return errors.WithMessage(err, "Unable to push digit")
		}
	}
	err = bd.refresh()
	if err != nil {
		return errors.WithMessage(err, "Unable to refresh display")
	}
	return nil
}
