module gitlab.com/heia-fr/bigdisplay/server

require (
	github.com/eclipse/paho.mqtt.golang v1.1.1
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	github.com/pkg/errors v0.8.0
	github.com/sirupsen/logrus v1.1.0
	github.com/stretchr/testify v1.2.2
	golang.org/x/crypto v0.0.0-20181001203147-e3636079e1a4 // indirect
	golang.org/x/net v0.0.0-20181005035420-146acd28ed58 // indirect
	golang.org/x/sys v0.0.0-20181005133103-4497e2df6f9e // indirect
	periph.io/x/periph v3.1.0+incompatible
)
