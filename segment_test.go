// Copyright 2018 Jacques Supcik
// Haute école d'ingénierie et d'architecture de Fribourg
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEncoding(t *testing.T) {
	s, err := textToSegments("HELLO", 6)
	assert.NoError(t, err, "Error")
	expected := []int{segment[' '], segment['H'], segment['E'], segment['L'], segment['L'], segment['O']}
	assert.EqualValues(t, expected, s, "Error")
}

func TestDotNumber(t *testing.T) {
	s, err := textToSegments("3.14159", 6)
	assert.NoError(t, err, "Error")
	expected := []int{segment['3'] | segment['.'], segment['1'], segment['4'], segment['1'], segment['5'], segment['9']}
	assert.EqualValues(t, expected, s, "Error")
}

func TestTooLong(t *testing.T) {
	_, err := textToSegments("0000000", 6)
	assert.Error(t, err, "Error")
}
func TestInvalidCharacter(t *testing.T) {
	_, err := textToSegments("0M0", 6)
	assert.Error(t, err, "Error")
	_, err = textToSegments("00M", 6)
	assert.Error(t, err, "Error")
	_, err = textToSegments("M00", 6)
	assert.Error(t, err, "Error")
}
